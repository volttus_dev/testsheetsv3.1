    OpenLayers.Feature.Vector.style.default = Object.assign({}, OpenLayers.Feature.Vector.style.default, {
        fillColor: "#ee9900",
        fillOpacity: 0.08,
        hoverFillColor: "white",
        hoverFillOpacity: 0.3,
        strokeColor: "Chartreuse",
        strokeOpacity: 1,
        strokeWidth: 2,
        strokeLinecap: "round",
        strokeDashstyle: "solid",
        hoverStrokeColor: "red",
        hoverStrokeOpacity: 1,
        hoverStrokeWidth: 0.2,
        hoverPointRadius: 1,
        hoverPointUnit: "%",
        pointerEvents: "visiblePainted",
        cursor: "inherit",
        fontColor: "#000000",
        labelAlign: "cm",
        labelOutlineColor: "white",
        labelOutlineWidth: 3
    });

    OpenLayers.Feature.Vector.style.select = Object.assign({}, OpenLayers.Feature.Vector.style.select, {
        fillColor: "#00BFFF",
        fillOpacity: 0.3,
        hoverFillColor: "white",
        hoverFillOpacity: 0.8,
        strokeColor: "#00BFFF",
        strokeOpacity: 0.3,
        strokeWidth: 2,
        strokeLinecap: "round",
        strokeDashstyle: "solid",
        hoverStrokeColor: "green",
        hoverStrokeOpacity: 1,
        hoverStrokeWidth: 0.2,
        pointRadius: 6,
        hoverPointRadius: 1,
        hoverPointUnit: "%",
        pointerEvents: "visiblePainted",
        cursor: "pointer",
        fontColor: "#000000",
        labelAlign: "cm",
        labelOutlineColor: "white",
        labelOutlineWidth: 3
    });

    OpenLayers.Control.SelectDevice = OpenLayers.Class(OpenLayers.Control.SelectFeature, {
        clickFeature: function(feature) {
            var selected = (OpenLayers.Util.indexOf(
                feature.layer.selectedFeatures, feature) > -1);
            if(selected) {
                if(this.toggleSelect()) {
                    this.unselect(feature);
                } else if(!this.multipleSelect()) {
                    this.unselectAll({except: feature});
                }
            } else {
                if(!this.multipleSelect()) {
                    this.unselectAll({except: feature});
                }
                this.select(feature);
            }
        },
    });

    OpenLayers.Control.AddDevice = OpenLayers.Class(OpenLayers.Control, {

        layer: null,
        callbacks: null,

        /**
         * APIProperty: events
         * {<OpenLayers.Events>} Events instance for listeners and triggering
         *     control specific events.
         *
         * Register a listener for a particular event with the following syntax:
         * (code)
         * control.events.register(type, obj, listener);
         * (end)
         *
         * Supported event types (in addition to those from <OpenLayers.Control.events>):
         * featureadded - Triggered when a feature is added
         */

        /**
         * APIProperty: multi
         * {Boolean} Cast features to multi-part geometries before passing to the
         *     layer.  Default is false.
         */
        multi: false,

        /**
         * APIProperty: featureAdded
         * {Function} Called after each feature is added
         */
        beforeFeatureAdded: function() {},

        /**
         * APIProperty: featureAdded
         * {Function} Called after each feature is added
         */
        featureAdded: function() {},

        /**
         * APIProperty: handlerOptions
         * {Object} Used to set non-default properties on the control's handler
         */

        /**
         * Constructor: OpenLayers.Control.AddDevice
         *
         * Parameters:
         * layer - {<OpenLayers.Layer.Vector>}
         * handler - {<OpenLayers.Handler>}
         * options - {Object}
         */
        initialize: function(layer, handler, options) {
            OpenLayers.Control.prototype.initialize.apply(this, [options]);
            this.callbacks = OpenLayers.Util.extend(
                {
                    done: this.drawFeature,
                    modify: function(vertex, feature) {
                        this.layer.events.triggerEvent(
                            "sketchmodified", {vertex: vertex, feature: feature}
                        );
                    },
                    create: function(vertex, feature) {
                        this.layer.events.triggerEvent(
                            "sketchstarted", {vertex: vertex, feature: feature}
                        );
                    }
                },
                this.callbacks
            );
            this.layer = layer;
            this.handlerOptions = this.handlerOptions || {};
            this.handlerOptions.layerOptions = OpenLayers.Util.applyDefaults(
                this.handlerOptions.layerOptions, {
                    renderers: layer.renderers, rendererOptions: layer.rendererOptions
                }
            );
            if (!("multi" in this.handlerOptions)) {
                this.handlerOptions.multi = this.multi;
            }
            var sketchStyle = this.layer.styleMap && this.layer.styleMap.styles.temporary;
            if(sketchStyle) {
                this.handlerOptions.layerOptions = OpenLayers.Util.applyDefaults(
                    this.handlerOptions.layerOptions,
                    {styleMap: new OpenLayers.StyleMap({"default": sketchStyle})}
                );
            }
            this.handler = new handler(this, this.callbacks, this.handlerOptions);
        },

        /**
         * Method: drawFeature
         */
        drawFeature: function(feature) {
            var feature = new OpenLayers.Feature.Vector(feature);
            var proceed = this.layer.events.triggerEvent(
                "sketchcomplete", {feature: feature}
            );
            if(proceed !== false) {
                feature.state = OpenLayers.State.INSERT;
                this.events.triggerEvent("beforefeatureadded", {feature: feature});
            }
        },

        /**
         * APIMethod: insertXY
         * Insert a point in the current sketch given x & y coordinates.
         *
         * Parameters:
         * x - {Number} The x-coordinate of the point.
         * y - {Number} The y-coordinate of the point.
         */
        insertXY: function(x, y) {
            if (this.handler && this.handler.line) {
                this.handler.insertXY(x, y);
            }
        },

        /**
         * APIMethod: insertDeltaXY
         * Insert a point given offsets from the previously inserted point.
         *
         * Parameters:
         * dx - {Number} The x-coordinate offset of the point.
         * dy - {Number} The y-coordinate offset of the point.
         */
        insertDeltaXY: function(dx, dy) {
            if (this.handler && this.handler.line) {
                this.handler.insertDeltaXY(dx, dy);
            }
        },

        /**
         * APIMethod: insertDirectionLength
         * Insert a point in the current sketch given a direction and a length.
         *
         * Parameters:
         * direction - {Number} Degrees clockwise from the positive x-axis.
         * length - {Number} Distance from the previously drawn point.
         */
        insertDirectionLength: function(direction, length) {
            if (this.handler && this.handler.line) {
                this.handler.insertDirectionLength(direction, length);
            }
        },

        /**
         * APIMethod: insertDeflectionLength
         * Insert a point in the current sketch given a deflection and a length.
         *     The deflection should be degrees clockwise from the previously
         *     digitized segment.
         *
         * Parameters:
         * deflection - {Number} Degrees clockwise from the previous segment.
         * length - {Number} Distance from the previously drawn point.
         */
        insertDeflectionLength: function(deflection, length) {
            if (this.handler && this.handler.line) {
                this.handler.insertDeflectionLength(deflection, length);
            }
        },

        /**
         * APIMethod: undo
         * Remove the most recently added point in the current sketch geometry.
         *
         * Returns:
         * {Boolean} An edit was undone.
         */
        undo: function() {
            return this.handler.undo && this.handler.undo();
        },

        /**
         * APIMethod: redo
         * Reinsert the most recently removed point resulting from an <undo> call.
         *     The undo stack is deleted whenever a point is added by other means.
         *
         * Returns:
         * {Boolean} An edit was redone.
         */
        redo: function() {
            return this.handler.redo && this.handler.redo();
        },

        /**
         * APIMethod: finishSketch
         * Finishes the sketch without including the currently drawn point.
         *     This method can be called to terminate drawing programmatically
         *     instead of waiting for the user to end the sketch.
         */
        finishSketch: function() {
            this.handler.finishGeometry();
        },

        /**
         * APIMethod: cancel
         * Cancel the current sketch.  This removes the current sketch and keeps
         *     the drawing control active.
         */
        cancel: function() {
            this.handler.cancel();
        },

        CLASS_NAME: "OpenLayers.Control.AddDevice"
    });

    OpenLayers.Control.DeleteDevice = OpenLayers.Class(OpenLayers.Control, {
        initialize: function(layer, options) {
            OpenLayers.Control.prototype.initialize.apply(this, [options]);
            this.layer = layer;
            this.handler = new OpenLayers.Handler.Feature(
                this, layer, {click: this.clickFeature}
            );
        },
        clickFeature: function(feature) {
            // if feature doesn't have a fid, destroy it
            if(feature.fid == undefined) {
                this.layer.destroyFeatures([feature]);
            } else {
                feature.state = OpenLayers.State.DELETE;
                this.layer.events.triggerEvent("afterfeaturemodified",
                    {feature: feature});
                feature.renderIntent = "select";
                this.layer.drawFeature(feature);
            }
        },
        setMap: function(map) {
            this.handler.setMap(map);
            OpenLayers.Control.prototype.setMap.apply(this, arguments);
        },
        CLASS_NAME: "OpenLayers.Control.DeleteDevice"
    });

    OpenLayers.Control.HighlightFeature = OpenLayers.Class(OpenLayers.Control.SelectFeature, {

        initialize: function(layers, options) {
            OpenLayers.Control.prototype.initialize.apply(this, [options]);
            if(this.scope === null) {
                this.scope = this;
            }
            this.initLayer(layers);
            var callbacks = {
                over : this.overFeature,
                out : this.outFeature,
                click: this.clickFeature,
                clickout: this.clickoutFeature
            };
            this.callbacks = OpenLayers.Util.extend(callbacks, this.callbacks);
            this.handlers = {
                feature: new OpenLayers.Handler.Feature(
                    this, this.layer, this.callbacks,
                    {geometryTypes: this.geometryTypes}
                )
            };
            if (this.box) {
                this.handlers.box = new OpenLayers.Handler.Box(
                    this, {done: this.selectBox},
                    {boxDivClassName: "olHandlerBoxSelectFeature"}
                );
            };
            this.selectStyle = OpenLayers.Feature.Vector.style.select;
            this.hover = false;
        },
        overFeature: function(feature) {
            var layer = feature.layer;
            if (feature){
                this.highlight(feature);
            }
        },
        outFeature: function(feature) {
            if(feature._lastHighlighter == this.id) {
                if(feature._prevHighlighter &&
                    feature._prevHighlighter != this.id) {
                    delete feature._lastHighlighter;
                    var control = this.map.getControl(
                        feature._prevHighlighter);
                    if(control) {
                        control.highlight(feature);
                    }
                } else {
                    this.unhighlight(feature);
                }
            }
        },
        CLASS_NAME: "OpenLayers.Control.HighlightFeature"
    });

    OpenLayers.Control.ComponentToolbar = OpenLayers.Class(
    OpenLayers.Control.Panel, {

        /**
         * APIProperty: citeCompliant
         * {Boolean} If set to true, coordinates of features drawn in a map extent
         * crossing the date line won't exceed the world bounds. Default is false.
         */
        citeCompliant: false,

        initialize: function(layer, options) {
            OpenLayers.Control.Panel.prototype.initialize.apply(this, [options]);

            this.highlightControl = new OpenLayers.Control.HighlightFeature(
                layer, {
                    geometryTypes: ['OpenLayers.Geometry.Polygon'],
                    title: "Highligt",
                    hover: true,
                    highlightOnly: true,
                    renderIntent: 'temporary',
                });

            this.navigationControl = new OpenLayers.Control.Navigation();
            this.selectionControl = new OpenLayers.Control.HighlightFeature(
                layer, {
                    geometryTypes: ['OpenLayers.Geometry.Polygon'],
                    title: "Select Device",
                    hover: true,
                    highlightOnly: true,
                    renderIntent: 'temporary',
                }
            );
            //var circleHandler = OpenLayers.Handler.RegularPolygon;
            var circleHandler = OpenLayers.Handler.Polygon;
            this.addDeviceControl = new OpenLayers.Control.AddDevice(layer, circleHandler, {
                displayClass: 'olControlAddDevicePoint',
                handlerOptions: {citeCompliant: this.citeCompliant},
                title: "Add Device",
                handlerOptions: {
                    sides: 10
                }
            });
            this.deleteDeviceControl = new OpenLayers.Control.DeleteDevice(
                layer, {title: "Delete Device"}
            );
            this.mouseControl = new OpenLayers.Control.MousePosition();
            var controls = [
                this.navigationControl,
                this.selectionControl,
            ];
            if (options.is_admin){
                controls.push(this.addDeviceControl)
                controls.push(this.deleteDeviceControl)
            }
            this.addControls(controls);
        },

        /**
         * Method: draw
         * calls the default draw, and then activates mouse defaults.
         *
         * Returns:
         * {DOMElement}
         */
        draw: function() {
            var div = OpenLayers.Control.Panel.prototype.draw.apply(this, arguments);
            if (this.defaultControl === null) {
                this.defaultControl = this.controls[0];
            }
            return div;
        },

        activeSelection: function(){
            this.deleteDeviceControl.deactivate();
            this.addDeviceControl.deactivate();
            this.navigationControl.deactivate();
            this.selectionControl.activate();
        },

        activeNavigation: function(){
            this.deleteDeviceControl.deactivate();
            this.addDeviceControl.deactivate();
            this.selectionControl.deactivate();
            this.navigationControl.activate();
        },

        CLASS_NAME: "OpenLayers.Control.ComponentToolbar"
    });

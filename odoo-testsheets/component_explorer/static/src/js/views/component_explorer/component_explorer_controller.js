odoo.define('component_explorer.ComponentExplorerController', function (require) {
    'use strict';
    var AbstractController = require('web.AbstractController');
    var BasicController = require('web.BasicController');
    var core = require('web.core');
    var qweb = core.qweb;
    var ComponentExplorerController = AbstractController.extend({
      custom_events: _.extend({}, BasicController.prototype.custom_events, {
         //Probando la propagación de eventos
         //pp: '_onPP',
      }),

      _onPP: function(event){
        alert('PP');
      },
    });

    return ComponentExplorerController;
});

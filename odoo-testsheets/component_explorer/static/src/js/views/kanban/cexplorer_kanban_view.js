odoo.define('component_explorer.CExplorerKanbanView', function (require) {
    'use strict';

    var KanbanView = require('web.KanbanView');

    var CExplorerKanbanView = KanbanView.extend({
       init: function(parent, viewInfo, params){
          this._super(viewInfo, params);
       },
    });

    return CExplorerKanbanView;

});
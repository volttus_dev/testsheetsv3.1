# -*- coding: utf-8 -*-
from odoo import api
from odoo import models, fields


class testsheets_curent_transformer(models.Model):
    _name = 'component_test.curent_transformer_test'
    _inherits = {'component_test.order': 'delegated_id'}

    _description = 'Current transformer'

    # columns
    delegated_id = fields.Many2one('component_test.order', required=True, ondelete='cascade')

    # ********** Visual & Mechanical Inspection*************************
    # Here the specific data of the Test
    Status1 = fields.Char('Status1', size=20, required=False)
    Status2 = fields.Char('Status2', size=20, required=False)
    Status3 = fields.Char('Status3', size=20, required=False)

    Notes1 = fields.Char('Notes1', size=40, required=False)
    Notes2 = fields.Char('Notes2', size=40, required=False)
    Notes3 = fields.Char('Notes3', size=40, required=False)

    # ********** Fiel data*************************
    SWGRId = fields.Char('SWGR Identification:', size=40, required=False)
    CurrentId = fields.Char('Current Identification:', size=40, required=False)

    CTCirc1 = fields.Char('CTCirc1', size=40, required=False)
    CTCirc2 = fields.Char('CTCirc2', size=40, required=False)
    CTCirc3 = fields.Char('CTCirc3', size=20, required=False)
    CTCirc4 = fields.Char('CTCirc4', size=20, required=False)

    Ratio1 = fields.Char('Ratio1', size=20, required=False)
    Ratio2 = fields.Char('Ratio2', size=20, required=False)
    Ratio3 = fields.Char('Ratio3', size=20, required=False)
    Ratio4 = fields.Char('Ratio4', size=20, required=False)

    Manuf1 = fields.Char('Manuf1', size=20, required=False)
    Manuf2 = fields.Char('Manuf2', size=20, required=False)
    Manuf3 = fields.Char('Manuf3', size=20, required=False)
    Manuf4 = fields.Char('Manuf4', size=20, required=False)

    Type1 = fields.Char('Type1', size=20, required=False)
    Type2 = fields.Char('Type2', size=20, required=False)
    Type3 = fields.Char('Type3', size=20, required=False)
    Type4 = fields.Char('Type4', size=20, required=False)

    BilR1 = fields.Char('BilR1', size=20, required=False)
    BilR2 = fields.Char('BilR2', size=20, required=False)
    BilR3 = fields.Char('BilR3', size=20, required=False)
    BilR4 = fields.Char('BilR4', size=20, required=False)

    CirId1 = fields.Char('CirId1', size=20, required=False)
    CirId2 = fields.Char('CirId2', size=20, required=False)
    CirId3 = fields.Char('CirId3', size=20, required=False)

    NamePR1 = fields.Float('NamePR1', default=0)
    NamePR2 = fields.Float('NamePR2', default=0)
    NamePR3 = fields.Float('NamePR3', default=0)

    MR1 = fields.Float('MR1', default=0)
    MR2 = fields.Float('MR2', default=0)
    MR3 = fields.Float('MR3', default=0)

    Polarity1 = fields.Char('Polarity1', size=20, required=False)
    Polarity2 = fields.Char('Polarity2', size=20, required=False)
    Polarity3 = fields.Char('Polarity3', size=20, required=False)

    CirCheck1 = fields.Char('CirCheck1', size=20, required=False)
    CirCheck2 = fields.Char('CirCheck2', size=20, required=False)
    CirCheck3 = fields.Char('CirCheck3', size=20, required=False)

    IRPrimary1 = fields.Integer('IRPrimary1', default=0)
    IRPrimary2 = fields.Integer('IRPrimary2', default=0)
    IRPrimary3 = fields.Integer('IRPrimary3', default=0)

    IRSecondary1 = fields.Integer('IRSecondary1', default=0)
    IRSecondary2 = fields.Integer('IRSecondary2', default=0)
    IRSecondary3 = fields.Integer('IRSecondary3', default=0)

    Notes4 = fields.Char('Notes4', size=20, required=False)
    Notes5 = fields.Char('Notes5', size=20, required=False)
    Notes6 = fields.Char('Notes6', size=20, required=False)

    # ********** Electrical Test: Saturations CT Data Points*************************
    CirDes1 = fields.Char('CirDes1', size=20, required=False)
    CirDes2 = fields.Char('CirDes2', size=20, required=False)
    CirDes3 = fields.Char('CirDes3', size=20, required=False)
    CirDes4 = fields.Char('CirDes4', size=20, required=False)
    CirDes5 = fields.Char('CirDes5', size=20, required=False)
    CirDes6 = fields.Char('CirDes6', size=20, required=False)
    CirDes7 = fields.Char('CirDes7', size=20, required=False)
    CirDes8 = fields.Char('CirDes8', size=20, required=False)
    CirDes9 = fields.Char('CirDes9', size=20, required=False)

    CurrentA1 = fields.Float('CurrentA1', default=0)
    CurrentA2 = fields.Float('CurrentA2', default=0)
    CurrentA3 = fields.Float('CurrentA3', default=0)
    CurrentA4 = fields.Float('CurrentA4', default=0)
    CurrentA5 = fields.Float('CurrentA5', default=0)
    CurrentA6 = fields.Float('CurrentA6', default=0)
    CurrentA7 = fields.Float('CurrentA7', default=0)
    CurrentA8 = fields.Float('CurrentA8', default=0)
    CurrentA9 = fields.Float('CurrentA9', default=0)

    VoltageA1 = fields.Float('VoltageA1', default=0)
    VoltageA2 = fields.Float('VoltageA2', default=0)
    VoltageA3 = fields.Float('VoltageA3', default=0)
    VoltageA4 = fields.Float('VoltageA4', default=0)
    VoltageA5 = fields.Float('VoltageA5', default=0)
    VoltageA6 = fields.Float('VoltageA6', default=0)
    VoltageA7 = fields.Float('VoltageA7', default=0)
    VoltageA8 = fields.Float('VoltageA8', default=0)
    VoltageA9 = fields.Float('VoltageA9', default=0)

    CurrentB1 = fields.Float('CurrentB1', default=0)
    CurrentB2 = fields.Float('CurrentB2', default=0)
    CurrentB3 = fields.Float('CurrentB3', default=0)
    CurrentB4 = fields.Float('CurrentB4', default=0)
    CurrentB5 = fields.Float('CurrentB5', default=0)
    CurrentB6 = fields.Float('CurrentB6', default=0)
    CurrentB7 = fields.Float('CurrentB7', default=0)
    CurrentB8 = fields.Float('CurrentB8', default=0)
    CurrentB9 = fields.Float('CurrentB9', default=0)

    VoltageB1 = fields.Float('VoltageB1', default=0)
    VoltageB2 = fields.Float('VoltageB2', default=0)
    VoltageB3 = fields.Float('VoltageB3', default=0)
    VoltageB4 = fields.Float('VoltageB4', default=0)
    VoltageB5 = fields.Float('VoltageB5', default=0)
    VoltageB6 = fields.Float('VoltageB6', default=0)
    VoltageB7 = fields.Float('VoltageB7', default=0)
    VoltageB8 = fields.Float('VoltageB8', default=0)
    VoltageB9 = fields.Float('VoltageB9', default=0)

    CurrentC1 = fields.Float('CurrentC1', default=0)
    CurrentC2 = fields.Float('CurrentC2', default=0)
    CurrentC3 = fields.Float('CurrentC3', default=0)
    CurrentC4 = fields.Float('CurrentC4', default=0)
    CurrentC5 = fields.Float('CurrentC5', default=0)
    CurrentC6 = fields.Float('CurrentC6', default=0)
    CurrentC7 = fields.Float('CurrentC7', default=0)
    CurrentC8 = fields.Float('CurrentC8', default=0)
    CurrentC9 = fields.Float('CurrentC9', default=0)

    VoltageC1 = fields.Float('VoltageC1', default=0)
    VoltageC2 = fields.Float('VoltageC2', default=0)
    VoltageC3 = fields.Float('VoltageC3', default=0)
    VoltageC4 = fields.Float('VoltageC4', default=0)
    VoltageC5 = fields.Float('VoltageC5', default=0)
    VoltageC6 = fields.Float('VoltageC6', default=0)
    VoltageC7 = fields.Float('VoltageC7', default=0)
    VoltageC8 = fields.Float('VoltageC8', default=0)
    VoltageC9 = fields.Float('VoltageC9', default=0)

    Burden1 = fields.Char('Burden1', size=20, required=False)
    Burden2 = fields.Char('Burden2', size=20, required=False)
    Burden3 = fields.Char('Burden3', size=20, required=False)
    Burden4 = fields.Char('Burden4', size=20, required=False)
    Burden5 = fields.Char('Burden5', size=20, required=False)
    Burden6 = fields.Char('Burden6', size=20, required=False)
    Burden7 = fields.Char('Burden7', size=20, required=False)
    Burden8 = fields.Char('Burden8', size=20, required=False)
    Burden9 = fields.Char('Burden9', size=20, required=False)

    # ********** Notes & Comments*************************
    Comments = fields.Text('Comments', required=False)

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        body = super(testsheets_curent_transformer, self).fields_view_get(view_id=view_id,
                                                                          view_type=view_type,
                                                                          toolbar=toolbar,
                                                                          submenu=submenu)
        return self.env['doc_report.doc'].set_readonly_form(body)

    @api.multi
    def write(self, vals):
        # if 'delegated_id' in vals:
        #     vals['component_id'] = vals['delegated_id']
        res = super(testsheets_curent_transformer, self).write(vals)
        return res

    @api.model
    @api.returns('self', lambda value: value.id)
    def create(self, vals):
        # self = self.with_context(alias_model_name='hr.equipment.request', alias_parent_model_name=self._name)
        if not vals.get('test_type') and 'test_type' not in self._context:
            vals['test_type'] = self._name
        return super(testsheets_curent_transformer, self).create(vals)
        # category_id.alias_id.write({'alias_parent_thread_id': category_id.id, 'alias_defaults': {'category_id': category_id.id}})
        # return category_id

    @api.multi
    def render_html(self, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('component_test.order')
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': self,
        }
        return report_obj.render('component_test.order', docargs)

    @api.multi
    def check_report(self, data=None):
        rec = self.browse(data)
        data = {}
        # data['form'] = rec.read(['location'])

        return self.env.ref('component_test.report_curent_transf_action').report_action(self, data=data)


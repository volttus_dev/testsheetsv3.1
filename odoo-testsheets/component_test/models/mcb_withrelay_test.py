# -*- coding: utf-8 -*-
from odoo import api
from odoo import models, fields


class testsheets_mcb_withrelay(models.Model):
    _name = 'component_test.mcb_withrelay_test'
    _inherits = {'component_test.order': 'delegated_id'}

    _description = 'Moduled Case Breaker(with relay)'
    # Columns
    delegated_id = fields.Many2one('component_test.order', required=True, ondelete='cascade')

    # ********** Mechanical & Electrical Inspection*************************

    Status1 = fields.Char('Status1', size=20, required=False)
    Status2 = fields.Char('Status2', size=20, required=False)
    Status3 = fields.Char('Status3', size=20, required=False)
    Status4 = fields.Char('Status4', size=20, required=False)
    Status5 = fields.Char('Status5', size=20, required=False)
    Status6 = fields.Char('Status6', size=20, required=False)
    Status7 = fields.Char('Status7', size=20, required=False)
    Status8 = fields.Char('Status8', size=20, required=False)
    Status9 = fields.Char('Status9', size=20, required=False)
    Status10 = fields.Char('Status10', size=20, required=False)
    Status11 = fields.Char('Status11', size=20, required=False)
    Status12 = fields.Char('Status12', size=20, required=False)
    Status13 = fields.Char('Status13', size=20, required=False)
    Status14 = fields.Char('Status14', size=20, required=False)
    Status15 = fields.Char('Status15', size=20, required=False)
    Status16 = fields.Char('Status16', size=20, required=False)

    Notes1 = fields.Char('Notes1', size=40, required=False)
    Notes2 = fields.Char('Notes2', size=40, required=False)
    Notes3 = fields.Char('Notes3', size=40, required=False)
    Notes4 = fields.Char('Notes4', size=40, required=False)
    Notes5 = fields.Char('Notes5', size=40, required=False)
    Notes6 = fields.Char('Notes6', size=40, required=False)
    Notes7 = fields.Char('Notes7', size=40, required=False)
    Notes8 = fields.Char('Notes8', size=40, required=False)
    Notes9 = fields.Char('Notes9', size=40, required=False)
    Notes10 = fields.Char('Notes10', size=40, required=False)
    Notes11 = fields.Char('Notes11', size=40, required=False)
    Notes12 = fields.Char('Notes12', size=40, required=False)
    Notes13 = fields.Char('Notes13', size=40, required=False)
    Notes14 = fields.Char('Notes14', size=40, required=False)
    Notes15 = fields.Char('Notes15', size=40, required=False)
    Notes16 = fields.Char('Notes16', size=40, required=False)

    # **********Relay Test & Settings:*************************
    TripUnit = fields.Char('Trip Unit:', size=40, required=False)
    Type2 = fields.Char('Type:', size=40, required=False)
    RatingPlug = fields.Char('Rating Plug:', size=40, required=False)
    TestEqp = fields.Char('Test Equipment:', size=40, required=False)
    TypeofInjection = fields.Char('Type o fInjection:', size=40, required=False)

    Rtest1 = fields.Char('Rtest1', size=20, required=False)
    Rtest2 = fields.Char('Rtest2', size=20, required=False)
    Rtest3 = fields.Char('Rtest3', size=20, required=False)
    Rtest4 = fields.Char('Rtest4', size=20, required=False)
    Rtest5 = fields.Char('Rtest5', size=20, required=False)

    Rtest6 = fields.Char('Rtest6', size=20, required=False)
    Rtest7 = fields.Char('Rtest7', size=20, required=False)
    Rtest8 = fields.Char('Rtest8', size=20, required=False)
    Rtest9 = fields.Char('Rtest9', size=20, required=False)
    Rtest10 = fields.Char('Rtest10', size=20, required=False)

    Rtest11 = fields.Char('Rtest11', size=20, required=False)
    Rtest12 = fields.Char('Rtest12', size=20, required=False)
    Rtest13 = fields.Char('Rtest13', size=20, required=False)
    Rtest14 = fields.Char('Rtest14', size=20, required=False)
    Rtest15 = fields.Char('Rtest15', size=20, required=False)

    Rtest16 = fields.Char('Rtest16', size=40, required=False)
    Rtest17 = fields.Char('Rtest17', size=40, required=False)
    Rtest18 = fields.Char('Rtest18', size=40, required=False)
    Rtest19 = fields.Char('Rtest19', size=40, required=False)
    Rtest20 = fields.Char('Rtest20', size=40, required=False)

    Rtest21 = fields.Char('Rtest21', size=40, required=False)
    Rtest22 = fields.Char('Rtest22', size=40, required=False)
    Rtest23 = fields.Char('Rtest23', size=40, required=False)
    Rtest24 = fields.Char('Rtest24', size=40, required=False)
    Rtest25 = fields.Char('Rtest25', size=40, required=False)

    Rtest26 = fields.Char('Rtest26', size=40, required=False)
    Rtest27 = fields.Char('Rtest27', size=40, required=False)
    Rtest28 = fields.Char('Rtest28', size=40, required=False)
    Rtest29 = fields.Char('Rtest29', size=40, required=False)
    Rtest30 = fields.Char('Rtest30', size=40, required=False)

    Rtest31 = fields.Char('Rtest31', size=40, required=False)
    Rtest32 = fields.Char('Rtest32', size=40, required=False)
    Rtest33 = fields.Char('Rtest33', size=40, required=False)
    Rtest34 = fields.Char('Rtest34', size=40, required=False)
    Rtest35 = fields.Char('Rtest35', size=40, required=False)

    # ********** Contact Resistance (µΩ):*************************
    CRPhaseA = fields.Char('PhaseA', size=40, required=False)
    CRPhaseB = fields.Char('PhaseB', size=40, required=False)
    CRPhaseC = fields.Char('PaseC', size=40, required=False)
    Close = fields.Char('Close', size=40, required=False)
    Trip = fields.Char('Trip', size=40, required=False)

    # **********Insulation Resistance (5000VDC(MΩ))*************************
    PhtoPhAB = fields.Char('PhtoPhAB', size=40, required=False)
    PhtoPhBC = fields.Char('PhtoPhBC', size=40, required=False)
    PhtoPhCA = fields.Char('PhtoPhCA', size=40, required=False)
    PhtoGndA = fields.Char('PhtoGndA', size=40, required=False)
    PhtoGndB = fields.Char('PhtoGndB', size=40, required=False)
    PhtoGndC = fields.Char('PhtoGndC', size=40, required=False)
    LtoLA = fields.Char('LtoLA', size=40, required=False)
    LtoLB = fields.Char('LtoLB', size=40, required=False)
    LtoLC = fields.Char('LtoLC', size=40, required=False)

    # ********** Notes & Comments*************************
    Comments = fields.Text('Comments', required=False)

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        body = super(testsheets_mcb_withrelay, self).fields_view_get(view_id=view_id,
                                                               view_type=view_type,
                                                               toolbar=toolbar,
                                                               submenu=submenu)
        return self.env['doc_report.doc'].set_readonly_form(body)



    @api.multi
    def write(self, vals):
        # if 'delegated_id' in vals:
        #     vals['component_id'] = vals['delegated_id']
        res = super(testsheets_mcb_withrelay, self).write(vals)
        return res

    @api.model
    @api.returns('self', lambda value: value.id)
    def create(self, vals):
        if not vals.get('test_type') and 'test_type' not in self._context:
            vals['test_type'] = self._name
        # self = self.with_context(alias_model_name='hr.equipment.request', alias_parent_model_name=self._name)
        return super(testsheets_mcb_withrelay, self).create(vals)
        # category_id.alias_id.write({'alias_parent_thread_id': category_id.id, 'alias_defaults': {'category_id': category_id.id}})
        # return category_id

    @api.multi
    def check_report(self, data=None):
        rec = self.browse(data)
        data = {}
        # data['form'] = rec.read(['location'])

        return self.env.ref('component_test.report_mcbac_withrelay_action').report_action(self, data=data)



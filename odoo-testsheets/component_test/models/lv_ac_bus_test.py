# -*- coding: utf-8 -*-
from odoo import api
from odoo import models, fields

class lv_ac_bus_test(models.Model):
    _name = 'component_test.lv_ac_bus_test'
    _inherits = {
        'component_test.order': 'delegated_id',
    }
    _description = 'LV Bus Test'

    # Columns
    delegated_id = fields.Many2one('component_test.order', required=True, ondelete='cascade')

    # ********** Mechanical & Electrical Inspection*************************
    Status1 = fields.Char('Status1', size=20, required=False)
    Status2 = fields.Char('Status2', size=20, required=False)
    Status3 = fields.Char('Status3', size=20, required=False)
    Status4 = fields.Char('Status4', size=20, required=False)
    Status5 = fields.Char('Status5', size=20, required=False)
    Status6 = fields.Char('Status6', size=20, required=False)
    Notes1 = fields.Char('Notes1', size=40, required=False)
    Notes2 = fields.Char('Notes2', size=40, required=False)
    Notes3 = fields.Char('Notes3', size=40, required=False)
    Notes4 = fields.Char('Notes4', size=40, required=False)
    Notes5 = fields.Char('Notes5', size=40, required=False)
    Notes6 = fields.Char('Notes6', size=40, required=False)


    # ********** Hi pot Test Reading of Leakage Current*************************

    TestVoltageA = fields.Integer('TestVoltageA', default=0)
    WConditionA = fields.Char('WConditionA', size=20, required=False)
    PhaseA1 = fields.Integer('PhaseA1', default=0)
    PhaseA2 = fields.Integer('PhaseA2', default=0)
    PhaseA3 = fields.Integer('PhaseA3', default=0)
    PhaseA4 = fields.Integer('PhaseA4', default=0)
    PhaseA5 = fields.Integer('PhaseA5', default=0)
    PhaseA6 = fields.Integer('PhaseA6', default=0)
    PhaseA7 = fields.Integer('PhaseA7', default=0)


    TestVoltageB = fields.Integer('TestVoltageB', default=0)
    WConditionB = fields.Char('WConditionB', size=20, required=False)
    PhaseB1 = fields.Integer('PhaseB1', default=0)
    PhaseB2 = fields.Integer('PhaseB2', default=0)
    PhaseB3 = fields.Integer('PhaseB3', default=0)
    PhaseB4 = fields.Integer('PhaseB4', default=0)
    PhaseB5 = fields.Integer('PhaseB5', default=0)
    PhaseB6 = fields.Integer('PhaseB6', default=0)
    PhaseB7 = fields.Integer('PhaseB7', default=0)

    TestVoltageC = fields.Integer('TestVoltageA', default=0)
    WConditionC = fields.Char('WConditionA', size=20, required=False)
    PhaseC1 = fields.Integer('PhaseC1', default=0)
    PhaseC2 = fields.Integer('PhaseC2', default=0)
    PhaseC3 = fields.Integer('PhaseC3', default=0)
    PhaseC4 = fields.Integer('PhaseC4', default=0)
    PhaseC5 = fields.Integer('PhaseC5', default=0)
    PhaseC6 = fields.Integer('PhaseC6', default=0)
    PhaseC7 = fields.Integer('PhaseC7', default=0)



    # ********** Insulation Resistance (MΩ) as per NETA Specifications*************************
    PhtoPhAB = fields.Integer('PhtoPhAB', default=0)
    PhtoPhBA = fields.Integer('PhtoPhBA', default=0)
    PhtoPhCA = fields.Integer('PhtoPhCA', default=0)
    PhtoGroundA = fields.Integer('PhtoGroundA', default=0)
    PhtoGroundB = fields.Integer('PhtoGroundB', default=0)
    PhtoGroundC = fields.Integer('PhtoGroundC', default=0)

    # ********** Notes & Comments*************************
    Comments = fields.Text('Comments', required=False)



    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        body = super(lv_ac_bus_test, self).fields_view_get(view_id=view_id,
                                                               view_type=view_type,
                                                               toolbar=toolbar,
                                                               submenu=submenu)
        return self.env['doc_report.doc'].set_readonly_form(body)


    @api.model
    @api.returns('self', lambda value: value.id)
    def create(self, vals):
        # self = self.with_context(alias_model_name='hr.equipment.request', alias_parent_model_name=self._name)
        if not vals.get('test_type') and 'test_type' not in self._context:
            vals['test_type'] = self._name
        return super(lv_ac_bus_test, self).create(vals)

    @api.multi
    def check_report(self, data=None):
        rec = self.browse(data)
        data = {}
        # data['form'] = rec.read(['location'])

        return self.env.ref('component_test.report_lvac_bus_action').report_action(self, data=data)



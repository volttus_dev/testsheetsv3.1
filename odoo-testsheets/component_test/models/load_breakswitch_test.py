# -*- coding: utf-8 -*-
from odoo import api
from odoo import models, fields


class testsheets_load_bs(models.Model):
    _name = 'component_test.load_bs_test'
    _inherits = {'component_test.order': 'delegated_id'}

    _description = 'Load Breaker Switch'
    # columns
    delegated_id = fields.Many2one('component_test.order', required=True, ondelete='cascade')

    Status1 = fields.Char('Status1', size=20, required=False)
    Status2 = fields.Char('Status2', size=20, required=False)
    Status3 = fields.Char('Status3', size=20, required=False)
    Status4 = fields.Char('Status4', size=20, required=False)
    Status5 = fields.Char('Status5', size=20, required=False)
    Status6 = fields.Char('Status6', size=20, required=False)
    Status7 = fields.Char('Status7', size=20, required=False)
    Status8 = fields.Char('Status8', size=20, required=False)
    Status9 = fields.Char('Status9', size=20, required=False)
    Status10 = fields.Char('Status10', size=20, required=False)
    Status11 = fields.Char('Status11', size=20, required=False)
    Status12 = fields.Char('Status12', size=20, required=False)
    Status13 = fields.Char('Status13', size=20, required=False)
    Status14 = fields.Char('Status14', size=20, required=False)
    Status15 = fields.Char('Status15', size=20, required=False)
    Status16 = fields.Char('Status16', size=20, required=False)


    Notes1 = fields.Char('Notes1', size=40, required=False)
    Notes2 = fields.Char('Notes2', size=40, required=False)
    Notes3 = fields.Char('Notes3', size=40, required=False)
    Notes4 = fields.Char('Notes4', size=40, required=False)
    Notes5 = fields.Char('Notes5', size=40, required=False)
    Notes6 = fields.Char('Notes6', size=40, required=False)
    Notes7 = fields.Char('Notes7', size=40, required=False)
    Notes8 = fields.Char('Notes8', size=40, required=False)
    Notes9 = fields.Char('Notes9', size=40, required=False)
    Notes10 = fields.Char('Notes10', size=40, required=False)
    Notes11 = fields.Char('Notes11', size=40, required=False)
    Notes12 = fields.Char('Notes12', size=40, required=False)
    Notes13 = fields.Char('Notes13', size=40, required=False)
    Notes14 = fields.Char('Notes14', size=40, required=False)
    Notes15 = fields.Char('Notes15', size=40, required=False)
    Notes16 = fields.Char('Notes16', size=40, required=False)



    # **********Fuse Data*************************
    Manufacturer2 = fields.Char('Manufacturer:', size=40, required=False)
    Disc = fields.Char('Disconnecting:', size=40, required=False)
    RatinA = fields.Float('Rating in Amp(A):', default=0)
    Alignment= fields.Char('Alignment:', size=40, required=False)
    HoldManuf = fields.Char('Holder Manufacturer:', size=40, required=False)
    Size = fields.Float('Size (A):', default=0)
    Mufflers = fields.Char('Mufflers:', size=40, required=False)
    MFS = fields.Float('Max Fuze Size (A):', default=0)
    FMC = fields.Char('Fuse Mount Condition:', size=40, required=False)
    CatalogNo = fields.Char('Catalog No.:', size=40, required=False)
    CableType = fields.Char('Cable Type:', size=40, required=False)
    Latch = fields.Char('Latch:', size=40, required=False)
    Type2 = fields.Char('Type:', size=40, required=False)
    Pothead = fields.Char('Pothead:', size=40, required=False)
    Voltage2 = fields.Float('Voltage (V):', default=0)
    StressCone = fields.Char('Stress Cone:', size=40, required=False)
    Bill = fields.Float('Bill (V):', default=0)


    # ********** Contact Resistance (µΩ):*************************
    PhaseA = fields.Integer('PhaseA', default=0)
    PhaseB = fields.Integer('PhaseB', default=0)
    PhaseC = fields.Integer('PaseC',  default=0)
    FuseA =  fields.Integer('FuseA',  default=0)
    FuseB =  fields.Integer('FuseB',  default=0)
    FuseC =  fields.Integer('FuseC',  default=0)

    # ********** Insulation Resistance (MΩ) as per NETA Specifications*************************
    PhtoPhAB =    fields.Integer('PhtoPhAB',    default=0)
    PhtoPhBA =    fields.Integer('PhtoPhBA',    default=0)
    PhtoPhCA =    fields.Integer('PhtoPhCA',    default=0)
    PhtoGroundA = fields.Integer('PhtoGroundA', default=0)
    PhtoGroundB = fields.Integer('PhtoGroundB', default=0)
    PhtoGroundC = fields.Integer('PhtoGroundC', default=0)
    PhtoPhAA =    fields.Integer('PhtoPhAA',    default=0)
    PhtoPhBB =    fields.Integer('PhtoPhBB',    default=0)
    PhtoPhCC =    fields.Integer('PhtoPhCC',    default=0)

    # ********** Hi Pot Test Reading of Leakage Current (µA)*************************
    TestVoltageA = fields.Integer('TestVoltageA', default=0)
    Duration = fields.Char('Duration', size=20, required=False)
    HPTPhtoGroundA = fields.Integer('HPTPhtoGroundA', default=0)
    HPTPhtoGroundB = fields.Integer('HPTPhtoGroundB', default=0)
    HPTPhtoGroundC = fields.Integer('HPTPhtoGroundC', default=0)

    # ********** Notes & Comments*************************
    Comments = fields.Text('Comments', required=False)

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        body = super(testsheets_load_bs, self).fields_view_get(view_id=view_id,
                                                                          view_type=view_type,
                                                                          toolbar=toolbar,
                                                                          submenu=submenu)
        return self.env['doc_report.doc'].set_readonly_form(body)

    @api.multi
    def write(self, vals):
        # if 'delegated_id' in vals:
        #     vals['component_id'] = vals['delegated_id']
        res = super(testsheets_load_bs, self).write(vals)
        return res

    @api.model
    @api.returns('self', lambda value: value.id)

    def create(self, vals):
        if not vals.get('test_type') and 'test_type' not in self._context:
            vals['test_type'] = self._name
        # self = self.with_context(alias_model_name='hr.equipment.request', alias_parent_model_name=self._name)
        return super(testsheets_load_bs, self).create(vals)
        # category_id.alias_id.write({'alias_parent_thread_id': category_id.id, 'alias_defaults': {'category_id': category_id.id}})
        # return category_id

    @api.multi
    def check_report(self, data=None):
        rec = self.browse(data)
        data = {}
        # data['form'] = rec.read(['location'])

        return self.env.ref('component_test.report_loadac_breakswitch_action').report_action(self, data=data)

    # @api.multi
    # def render_html(self, data=None):
    #     report_obj = self.env['report']
    #     report = report_obj._get_report_from_name('component_test.order')
    #     docargs = {
    #         'doc_ids': self._ids,
    #         'doc_model': report.model,
    #         'docs': self,
    #     }
    #     return report_obj.render('component_test.order', docargs)
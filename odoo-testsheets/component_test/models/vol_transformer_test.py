# -*- coding: utf-8 -*-
from odoo import api
from odoo import models, fields


class testsheets_vol_transformer(models.Model):
    _name = 'component_test.vol_transformer_test'
    _inherits = {'component_test.order': 'delegated_id'}

    _description = 'Volt transformer'
    # Columns
    delegated_id = fields.Many2one('component_test.order', required=True, ondelete='cascade')

    # ********** Visual & Mechanical Inspection*************************
    # Here the specific data of the Test
    Status1 = fields.Char('Status1', size=20, required=False)
    Status2 = fields.Char('Status2', size=20, required=False)
    Status3 = fields.Char('Status3', size=20, required=False)

    Notes1 = fields.Char('Notes1', size=40, required=False)
    Notes2 = fields.Char('Notes2', size=40, required=False)
    Notes3 = fields.Char('Notes3', size=40, required=False)

    # *****************Electrical test******************
    PrA = fields.Integer('PrA', default=0)
    PrB = fields.Integer('PrB', default=0)
    PrC = fields.Integer('PrC', default=0)
    SecA = fields.Integer('SecA', default=0)
    SecB = fields.Integer('SecB', default=0)
    SecC = fields.Integer('SecC', default=0)
    FuseA = fields.Integer('FuseA', default=0)
    FuseB = fields.Integer('FuseB', default=0)
    FuseC = fields.Integer('FuseC', default=0)

    BurdenAB = fields.Float('BurdenAB', default=0)
    BurdenBC = fields.Float('BurdenBC', default=0)
    BurdenCA = fields.Float('BurdenCA', default=0)

    # ***************Ratio and Polarity*************************************
    PTId = fields.Char('PTId', size=40, required=False)
    NamePRatio = fields.Char('NamePRatio', size=40, required=False)
    PhaseA = fields.Float('PhaseA', default=0)
    mA = fields.Integer('mA', size=40, default=0)
    PhaseB = fields.Float('PhaseB', default=0)
    mB = fields.Integer('mB', default=0)
    PhaseC = fields.Float('PhaseC', default=0)
    mC = fields.Integer('mC', default=0)
    PolarityA = fields.Char('PolarityA', size=40, required=False)
    PolarityB = fields.Char('PolarityB', size=40, required=False)
    PolarityC = fields.Char('PolarityC', size=40, required=False)

    # ********** Insulation Resistan as per NETA Specifications************************
    InsRest1 =  fields.Integer('InsRest1',  default=0)
    InsRest2 =  fields.Integer('InsRest2',  default=0)
    InsRest3 =  fields.Integer('InsRest3',  default=0)
    InsRest4 =  fields.Integer('InsRest4',  default=0)
    InsRest5 =  fields.Integer('InsRest5',  default=0)
    InsRest6 =  fields.Integer('InsRest6',  default=0)
    InsRest7 =  fields.Integer('InsRest7',  default=0)
    InsRest8 =  fields.Integer('InsRest8',  default=0)
    InsRest9 =  fields.Integer('InsRest9',  default=0)
    InsRest10 = fields.Integer('InsRest10', default=0)
    InsRest11 = fields.Integer('InsRest11', default=0)
    InsRest12 = fields.Integer('InsRest12', default=0)

    # ********** Notes & Comments*************************
    Comments = fields.Text('Comments', required=False)

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        body = super(testsheets_vol_transformer, self).fields_view_get(view_id=view_id,
                                                                      view_type=view_type,
                                                                      toolbar=toolbar,
                                                                      submenu=submenu)
        return self.env['doc_report.doc'].set_readonly_form(body)

    @api.multi
    def write(self, vals):
        # if 'delegated_id' in vals:
        #     vals['component_id'] = vals['delegated_id']
        res = super(testsheets_vol_transformer, self).write(vals)
        return res

    @api.model
    @api.returns('self', lambda value: value.id)
    def create(self, vals):
        # self = self.with_context(alias_model_name='hr.equipment.request', alias_parent_model_name=self._name)
        if not vals.get('test_type') and 'test_type' not in self._context:
            vals['test_type'] = self._name
        return super(testsheets_vol_transformer, self).create(vals)
        # category_id.alias_id.write({'alias_parent_thread_id': category_id.id, 'alias_defaults': {'category_id': category_id.id}})
        # return category_id

    @api.multi
    def render_html(self, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('component_test.order')
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': self,
        }
        return report_obj.render('component_test.order', docargs)

    @api.multi
    def check_report(self, data=None):
        rec = self.browse(data)
        data = {}
        # data['form'] = rec.read(['location'])

        return self.env.ref('component_test.report_vol_transf_action').report_action(self, data=data)


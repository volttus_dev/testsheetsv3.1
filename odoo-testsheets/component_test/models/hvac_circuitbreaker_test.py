# -*- coding: utf-8 -*-
from odoo import api
from odoo import models, fields

class testsheets_hvac_cb(models.Model):
    _name = 'component_test.hvac_circuitbreaker_test'
    _inherits = {'component_test.order': 'delegated_id'}

    _description = 'HV AC CIRCUIT BREAKER'
    # columns
    delegated_id = fields.Many2one('component_test.order', required=True, ondelete='cascade')

    # ********** Mechanical & Electrical Inspection*************************
    Status1 = fields.Char('Status1', size=20, required=False)
    Status2 = fields.Char('Status2', size=20, required=False)
    Status3 = fields.Char('Status3', size=20, required=False)
    Status4 = fields.Char('Status4', size=20, required=False)
    Status5 = fields.Char('Status5', size=20, required=False)
    Status6 = fields.Char('Status6', size=20, required=False)
    Status7 = fields.Char('Status7', size=20, required=False)
    Status8 = fields.Char('Status8', size=20, required=False)
    Status9 = fields.Char('Status9', size=20, required=False)
    Status10 = fields.Char('Status10', size=20, required=False)
    Status11 = fields.Char('Status11', size=20, required=False)
    Status12 = fields.Char('Status12', size=20, required=False)
    Status13 = fields.Char('Status13', size=20, required=False)
    Status14 = fields.Char('Status14', size=20, required=False)
    Status15 = fields.Char('Status15', size=20, required=False)
    Status16 = fields.Char('Status16', size=20, required=False)
    Status17 = fields.Char('Status17', size=20, required=False)
    Status18 = fields.Char('Status18', size=20, required=False)
    Status19 = fields.Char('Status19', size=20, required=False)
    Status20 = fields.Char('Status20', size=20, required=False)
    Notes1 = fields.Char('Notes1', size=40, required=False)
    Notes2 = fields.Char('Notes2', size=40, required=False)
    Notes3 = fields.Char('Notes3', size=40, required=False)
    Notes4 = fields.Char('Notes4', size=40, required=False)
    Notes5 = fields.Char('Notes5', size=40, required=False)
    Notes6 = fields.Char('Notes6', size=40, required=False)
    Notes7 = fields.Char('Notes7', size=40, required=False)
    Notes8 = fields.Char('Notes8', size=40, required=False)
    Notes9 = fields.Char('Notes9', size=40, required=False)
    Notes10 = fields.Char('Notes10', size=40, required=False)
    Notes11 = fields.Char('Notes11', size=40, required=False)
    Notes12 = fields.Char('Notes12', size=40, required=False)
    Notes13 = fields.Char('Notes13', size=40, required=False)
    Notes14 = fields.Char('Notes14', size=40, required=False)
    Notes15 = fields.Char('Notes15', size=40, required=False)
    Notes16 = fields.Char('Notes16', size=40, required=False)
    Notes17 = fields.Char('Notes17', size=40, required=False)
    Notes18 = fields.Char('Notes18', size=40, required=False)
    Notes19 = fields.Char('Notes19', size=40, required=False)
    Notes20 = fields.Char('Notes20', size=40, required=False)
    CountF = fields.Integer('CountF', default=0)
    AsLeft = fields.Integer('AsLeft', default=0)

    # ********** Contact Resistance (µΩ):*************************
    PhaseA = fields.Integer('PhaseA',  default=0)
    PhaseB = fields.Integer('PhaseB',  default=0)
    PhaseC = fields.Integer('PhaseC',  default=0)

    # **********Insulation Resistance (5000VDC(MΩ))*************************
    PhtoPhAB = fields.Integer('PhtoPhAB', default=0)
    PhtoPhBC = fields.Integer('PhtoPhBC', default=0)
    PhtoPhCA = fields.Integer('PhtoPhCA', default=0)
    PhtoGndA = fields.Integer('PhtoGndA', default=0)
    PhtoGndB = fields.Integer('PhtoGndB', default=0)
    PhtoGndC = fields.Integer('PhtoGndC', default=0)
    LtoLA = fields.Integer('LtoLA', default=0)
    LtoLB = fields.Integer('LtoLB', default=0)
    LtoLC = fields.Integer('LtoLC', default=0)

    # ********** Hi Pot Test Reading of Leakage Current (µA):*************************
    CurPhtoPhAB = fields.Integer('CurPhtoPhAB', default=0)
    CurPhtoPhBC = fields.Integer('CurPhtoPhBC', default=0)
    CurPhtoPhCA = fields.Integer('CurPhtoPhCA', default=0)
    CurPhtoGndA = fields.Integer('CurPhtoGndA', default=0)
    CurPhtoGndB = fields.Integer('CurPhtoGndB', default=0)
    CurPhtoGndC = fields.Integer('CurPhtoGndC', default=0)
    CurLtoLA = fields.Integer('CurLtoLA', default=0)
    CurLtoLB = fields.Integer('CurLtoLB', default=0)
    CurLtoLC = fields.Integer('CurLtoLC', default=0)

    # ********** Notes & Comments*************************
    Comments = fields.Text('Comments', required=False)


    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        body = super(testsheets_hvac_cb, self).fields_view_get(view_id=view_id,
                                                                 view_type=view_type,
                                                                 toolbar=toolbar,
                                                                 submenu=submenu)
        return self.env['doc_report.doc'].set_readonly_form(body)


    @api.multi
    def write(self, vals):
    # if 'delegated_id' in vals:
        #     vals['component_id'] = vals['delegated_id']
        res = super(testsheets_hvac_cb, self).write(vals)
        return res

    @api.model
    @api.returns('self', lambda value: value.id)
    def create(self, vals):
        # self = self.with_context(alias_model_name='hr.equipment.request', alias_parent_model_name=self._name)
        if not vals.get('test_type') and 'test_type' not in self._context:
            vals['test_type'] = self._name
        return super(testsheets_hvac_cb, self).create(vals)
        # category_id.alias_id.write({'alias_parent_thread_id': category_id.id, 'alias_defaults': {'category_id': category_id.id}})
        # return category_id

    @api.multi
    def check_report(self, data=None):
        rec = self.browse(data)
        data = {}
        # data['form'] = rec.read(['location'])

        return self.env.ref('component_test.report_hvac_circuitbreaker_action').report_action(self, data=data)

# -*- coding: utf-8 -*-
from odoo import api
from odoo import models, fields


class testsheets_hv_busjcr(models.Model):
    _name = 'component_test.hv_busjcr_test'
    _inherits = {'component_test.order': 'delegated_id'}

    _description = 'HV Bus Joint Contact Resistance'

    # columns
    delegated_id = fields.Many2one('component_test.order', required=True, ondelete='cascade')
    component_id = fields.Many2one('component.hv_busjcr', required=True, string='Component inherited', readonly=True)

    Equip_Designation = fields.Char(realated='component_id.Equip_Designation', string='Equip_Designation', readonly=True)

    # ********** Mechanical & Electrical Inspection*************************
    # Here the specific data of the Test
    TPa1 = fields.Char('TPa1', size=20, required=False)
    TPb1 = fields.Char('TPa1', size=20, required=False)
    Phasea1 = fields.Char('Phasea1', size=20, required=False)
    Phaseb1 = fields.Char('Phaseb1', size=20, required=False)
    Phasec1 = fields.Char('Phasec1', size=20, required=False)
    Current1 = fields.Char('Current1', size=20, required=False)

    TPa2 = fields.Char('TPa2', size=20, required=False)
    TPb2 = fields.Char('TPa2', size=20, required=False)
    Phasea2 = fields.Char('Phasea2', size=20, required=False)
    Phaseb2 = fields.Char('Phaseb2', size=20, required=False)
    Phasec2 = fields.Char('Phasec2', size=20, required=False)
    Current2 = fields.Char('Current2', size=20, required=False)

    TPa3 = fields.Char('TPa3', size=20, required=False)
    TPb3 = fields.Char('TPa3', size=20, required=False)
    Phasea3 = fields.Char('Phasea3', size=20, required=False)
    Phaseb3 = fields.Char('Phaseb3', size=20, required=False)
    Phasec3 = fields.Char('Phasec3', size=20, required=False)
    Current3 = fields.Char('Current3', size=20, required=False)

    TPa4 = fields.Char('TPa4', size=20, required=False)
    TPb4 = fields.Char('TPa4', size=20, required=False)
    Phasea4 = fields.Char('Phasea4', size=20, required=False)
    Phaseb4 = fields.Char('Phaseb4', size=20, required=False)
    Phasec4 = fields.Char('Phasec4', size=20, required=False)
    Current4 = fields.Char('Current4', size=20, required=False)

    TPa5 = fields.Char('TPa5', size=20, required=False)
    TPb5 = fields.Char('TPa5', size=20, required=False)
    Phasea5 = fields.Char('Phasea5', size=20, required=False)
    Phaseb5 = fields.Char('Phaseb5', size=20, required=False)
    Phasec5 = fields.Char('Phasec5', size=20, required=False)
    Current5 = fields.Char('Current5', size=20, required=False)

    TPa6 = fields.Char('TPa6', size=20, required=False)
    TPb6 = fields.Char('TPa6', size=20, required=False)
    Phasea6 = fields.Char('Phasea6', size=20, required=False)
    Phaseb6 = fields.Char('Phaseb6', size=20, required=False)
    Phasec6 = fields.Char('Phasec6', size=20, required=False)
    Current6 = fields.Char('Current6', size=20, required=False)

    TPa7 = fields.Char('TPa7', size=20, required=False)
    TPb7 = fields.Char('TPa7', size=20, required=False)
    Phasea7 = fields.Char('Phasea7', size=20, required=False)
    Phaseb7 = fields.Char('Phaseb7', size=20, required=False)
    Phasec7 = fields.Char('Phasec7', size=20, required=False)
    Current7 = fields.Char('Current7', size=20, required=False)

    TPa8 = fields.Char('TPa8', size=20, required=False)
    TPb8 = fields.Char('TPa8', size=20, required=False)
    Phasea8 = fields.Char('Phasea8', size=20, required=False)
    Phaseb8 = fields.Char('Phaseb8', size=20, required=False)
    Phasec8 = fields.Char('Phasec8', size=20, required=False)
    Current8 = fields.Char('Current8', size=20, required=False)

    TPa9 = fields.Char('TPa9', size=20, required=False)
    TPb9 = fields.Char('TPa9', size=20, required=False)
    Phasea9 = fields.Char('Phasea9', size=20, required=False)
    Phaseb9 = fields.Char('Phaseb9', size=20, required=False)
    Phasec9 = fields.Char('Phasec9', size=20, required=False)
    Current9 = fields.Char('Current9', size=20, required=False)

    TPa10 = fields.Char('TPa10', size=20, required=False)
    TPb10 = fields.Char('TPa10', size=20, required=False)
    Phasea10 = fields.Char('Phasea10', size=20, required=False)
    Phaseb10 = fields.Char('Phaseb10', size=20, required=False)
    Phasec10 = fields.Char('Phasec10', size=20, required=False)
    Current10 = fields.Char('Current10', size=20, required=False)

    TPa11 = fields.Char('TPa11', size=20, required=False)
    TPb11 = fields.Char('TPa11', size=20, required=False)
    Phasea11 = fields.Char('Phasea11', size=20, required=False)
    Phaseb11 = fields.Char('Phaseb11', size=20, required=False)
    Phasec11 = fields.Char('Phasec11', size=20, required=False)
    Current11 = fields.Char('Current11', size=20, required=False)

    TPa12 = fields.Char('TPa12', size=20, required=False)
    TPb12 = fields.Char('TPa12', size=20, required=False)
    Phasea12 = fields.Char('Phasea12', size=20, required=False)
    Phaseb12 = fields.Char('Phaseb12', size=20, required=False)
    Phasec12 = fields.Char('Phasec12', size=20, required=False)
    Current12 = fields.Char('Current12', size=20, required=False)

    TPa13 = fields.Char('TPa13', size=20, required=False)
    TPb13 = fields.Char('TPa13', size=20, required=False)
    Phasea13 = fields.Char('Phasea13', size=20, required=False)
    Phaseb13 = fields.Char('Phaseb13', size=20, required=False)
    Phasec13 = fields.Char('Phasec13', size=20, required=False)
    Current13 = fields.Char('Current13', size=20, required=False)

    TPa14 = fields.Char('TPa14', size=20, required=False)
    TPb14 = fields.Char('TPa14', size=20, required=False)
    Phasea14 = fields.Char('Phasea14', size=20, required=False)
    Phaseb14 = fields.Char('Phaseb14', size=20, required=False)
    Phasec14 = fields.Char('Phasec14', size=20, required=False)
    Current14 = fields.Char('Current14', size=20, required=False)

    TPa15 = fields.Char('TPa15', size=20, required=False)
    TPb15 = fields.Char('TPa15', size=20, required=False)
    Phasea15 = fields.Char('Phasea15', size=20, required=False)
    Phaseb15 = fields.Char('Phaseb15', size=20, required=False)
    Phasec15 = fields.Char('Phasec15', size=20, required=False)
    Current15 = fields.Char('Current15', size=20, required=False)


    # ********** Notes & Comments*************************
    Comments = fields.Text('Comments', required=False)



    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        body = super(testsheets_hv_busjcr, self).fields_view_get(view_id=view_id,
                                                                            view_type=view_type,
                                                                            toolbar=toolbar,
                                                                            submenu=submenu)
        return self.env['doc_report.doc'].set_readonly_form(body)

    @api.multi
    def write(self, vals):
        # if 'delegated_id' in vals:
        #     vals['component_id'] = vals['delegated_id']
        res = super(testsheets_hv_busjcr, self).write(vals)
        return res

    @api.model
    @api.returns('self', lambda value: value.id)
    def create(self, vals):
        # self = self.with_context(alias_model_name='hr.equipment.request', alias_parent_model_name=self._name)
        return super(testsheets_hv_busjcr, self).create(vals)
        # category_id.alias_id.write({'alias_parent_thread_id': category_id.id, 'alias_defaults': {'category_id': category_id.id}})
        # return category_id

    @api.multi
    def render_html(self, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('component_test.order')
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': self,
        }
        return report_obj.render('component_test.order', docargs)
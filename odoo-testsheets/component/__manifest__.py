# -*- encoding: utf-8 -*-
##############################################################################
#
#    Samples module for Odoo Web Login Screen
#    Copyright (C) 2018- XUBI.ME (http://www.xubi.me)
#    @author binhnguyenxuan (https://www.linkedin.com/in/binhnguyenxuan)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#    
#    Background Source: http://forum.xda-developers.com/showpost.php?p=37322378
#
##############################################################################
{
    'name': 'VA Component',
    'version': '1.9',
    'summary': 'Components Management',
    'description': """
Managing Components in Odoo.
===========================
Support following feature:
    * Location for Component
    * Assign Component to employee
    * Track warranty information
    * Custom states of Component
    * States of Component for different team: Finance, Warehouse, Manufacture, Maintenance and Accounting
    * Drag&Drop manage states of Component
    * Component Tags
    * Search by main fields
    """,
    'author': 'Voltus Automation',
    'website': 'http://www.voltusautomation.com',
    'category': 'Enterprise Component Management',
    'sequence': 0,
    'images': ['images/component.png'],
    'depends': [
                'mail',
                'web',
                'component_explorer',
    ],
    # 'demo': ['component_demo.xml'],
    'data': [
        'security/component_security.xml',
        'security/ir.model.access.csv',
        'views/component_explorer.xml',
        'views/component_view.xml',
        'views/component_view_ptop_ground.xml',
        'views/component_view_lv_ac_cable.xml',
        'views/component_view_board.xml',
        'views/component_view_lv_ac_bus.xml',
        'views/component_view_hv_ac_bus.xml',
        'views/component_view_hv_busjcr.xml',
        'views/component_view_hv_ac_cable.xml',
        'views/component_view_lv_dc_cable.xml',
        'views/component_view_hvac_circuitbreaker.xml',
        'views/component_view_dry_transformer.xml',
        'views/component_view_vol_transformer.xml',
        'views/component_view_curent_transformer.xml',
        'views/component_view_pindex_transformer.xml',
        'views/component_view_surge_arrestor.xml',
        'views/component_view_liquid_transformer.xml',
        'views/component_view_automatic_transferswitch.xml',
        'views/component_view_lv_circuitbreaker.xml',
        'views/component_view_lv_circuitbreakereatonamp.xml',
        'views/component_view_lvdc_circuitbreaker.xml',
        'views/component_site_templates.xml',
        'component_data.xml',
        'views/site_view.xml',
        'views/component.xml',
        'views/project_view.xml',
        'views/component_view_mcb_withrelay.xml',
        'views/component_view_load_breakswitch.xml',
        'views/contact_view.xml'
    ],
    'installable': True,
    'application': True,
}
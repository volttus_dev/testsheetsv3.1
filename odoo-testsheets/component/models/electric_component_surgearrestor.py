# -*- coding: utf-8 -*-
from odoo import api
from .electric_component import electric_component_base
from odoo import fields, models

ASSET_TYPE = 'surge_arrestor'
ASSET_MODEL = 'component.surge_arrestor'
ASSET_MODEL_TEST = 'component_test.surge_arrestor_test'


class ElectricComponentSurgeArrestor(electric_component_base):
    _name = ASSET_MODEL

    _inherits = {
        'component.component': 'delegated_id',
    }

    def _component_info(self):
        res = dict.fromkeys(self.ids, "")
        return res

    # Columns
    delegated_id = fields.Many2one('component.component', required=True, ondelete='cascade')

    # ************* Equipment Data. Specific fields of Surge Arrestor ***************
    Equip_Designation = fields.Char('Equip_Designation', size=40, required=False)
    Substation = fields.Char('Substation', size=40, required=False)

    component_info = fields.Char(compute='_component_info', string='Information')

    @api.model
    def create(self, values):
        if not values.get('electric_component_type') and 'electric_component_type' not in self._context:
            values['electric_component_type'] = ASSET_TYPE
            values['component_model'] = ASSET_MODEL
            values['component_model_test'] = ASSET_MODEL_TEST
        return super(ElectricComponentSurgeArrestor, self).create(values)


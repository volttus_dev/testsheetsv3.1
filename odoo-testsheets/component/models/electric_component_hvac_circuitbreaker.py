# -*- coding: utf-8 -*-
from odoo import api
from .electric_component import electric_component_base
from odoo import fields, models

ASSET_TYPE = 'hvac_circuitbreaker'
ASSET_MODEL = 'component.hvac_circuitbreaker'
ASSET_MODEL_TEST = 'component_test.hvac_circuitbreaker_test'


class HvAcCircuitBreakerComponent(electric_component_base):
    _name = ASSET_MODEL
    _inherits = {'component.component': 'delegated_id',}

    def _component_info(self):
        res = dict.fromkeys(self.ids, "")
        for rec in self:
            value = ' V=' + str(rec.V) + ' ,'
            value = value + 'Motor Op V=' + str(rec.MotorOpV) + ' ,'
            value = value + 'Close Coil V=' + str(rec.ClosingCoilV)
            res[self.ids[0]] = value
        return res

    # Columns
    delegated_id = fields.Many2one('component.component', required=True, ondelete='cascade')
    operation = fields.Selection([('main','Main'),('tie','Tie'),('feeder','Feeder')], 'Breaker operation')

    # ********** Equipment Data*************************-
    Manufacturer = fields.Char('Manufacturer', size=50, required=False)
    MotorOpV = fields.Float('MotorOpV', default=0)
    ClosingCoilV = fields.Float('ClosingCoilV', default=0)
    TrippingCoilV = fields.Float('TrippingCoilV', default=0)
    Type = fields.Char('Type', size=20, required=False)
    V = fields.Float('V', default=0)
    Bil = fields.Char('Bil', size=20, required=False)
    MFGDate = fields.Date('MFGDate', required=False)
    Serial = fields.Char('Serial', size=20, required=False)
    Frame = fields.Integer('Frame', default=0)
    KFactor = fields.Char('KFactor', size=20, required=False)
    BreakerWt = fields.Char('BreakerWt', size=20, required=False)
    StyleOrder = fields.Char('StyleOrder', size=20, required=False)
    InterruptTime = fields.Char('InterruptTime', size=20, required=False)
    ShortCirCur = fields.Char('ShortCirCur', size=20, required=False)
    InterLeaflet = fields.Char('InterLeaflet', size=20, required=False)
    component_info = fields.Char(compute='_component_info', string='Information')
    Equip_Designation = fields.Char('Equip_Designation', size=40, required=False)

    _defaults = {
        'operation': 'main',
    }

    @api.multi
    def write(self, vals):
        # self._test_image_small(vals)
        vals = self.update_operation(vals)
        res = super(HvAcCircuitBreakerComponent, self).write(vals)
        return res

    def update_operation(self, vals):
        if ('operation' in vals):
            if (vals['operation'] != False):
                if vals['operation'] == 'main':
                    vals['color'] = 2
                elif vals['operation'] == 'tie':
                    vals['color'] = 3
                else:
                    vals['color'] = 4
        return vals

    @api.model
    def create(self, values):
        if not values.get('electric_component_type') and 'electric_component_type' not in self._context:
            values['electric_component_type'] = ASSET_TYPE
            values['component_model'] = ASSET_MODEL
            values['component_model_test'] = ASSET_MODEL_TEST
        values = self.update_operation(values)
        return super(HvAcCircuitBreakerComponent, self).create(values)




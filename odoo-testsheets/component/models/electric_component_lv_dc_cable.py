# -*- coding: utf-8 -*-
from odoo import api
from .electric_component import electric_component_base
from odoo import fields, models

ASSET_TYPE = 'lv_dc_cable'
ASSET_MODEL = 'component.lv_dc_cable'
ASSET_MODEL_TEST = 'component_test.lv_dc_cable_test'


class ElectricCabledcComponent(electric_component_base):
    _name = ASSET_MODEL

    _inherits = {
        'component.component': 'delegated_id',
    }

    def _component_info(self):
        res = dict.fromkeys(self.ids, "")
        for rec in self:
            value = 'AWG=' + str(rec.awg) + ' ,'
            value = value + 'Per phase=' + str(rec.per_phase)
            res[self.ids[0]] = value
        return res

    # Columns
    delegated_id = fields.Many2one('component.component', required=True, ondelete='cascade')

    # ************* Equipment Data. Specific fields of Dry Transformers ***************
    from_node = fields.Char('From<--', size=50, required=False)
    to_node = fields.Char('To-->', size=50, required=False)
    awg = fields.Float('AWG', digits=0, required=False)
    per_phase = fields.Integer('Per phase', required=False)
    component_info = fields.Char(compute='_component_info', string='Information')

    @api.model
    def create(self, values):
        if not values.get('electric_component_type') and 'electric_component_type' not in self._context:
            values['electric_component_type'] = ASSET_TYPE
            values['component_model'] = ASSET_MODEL
            values['component_model_test'] = ASSET_MODEL_TEST
        return super(ElectricCabledcComponent, self).create(values)


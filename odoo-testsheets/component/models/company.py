from odoo import models


class VACompany(models.Model):
    _inherit = 'res.company'

    def _get_usd(self, cr, uid, context=None):
        rate_obj = self.pool.get('res.currency')
        curr = rate_obj.search(cr, uid, [('name', '=', 'USD')], context=context)
        return curr[0]

    _defaults = {
        'currency_id': _get_usd,
    }
# -*- coding: utf-8 -*-
from odoo import api
from .electric_component import electric_component_base
from odoo import fields, models

ASSET_TYPE = 'liquid_transformer'
ASSET_MODEL = 'component.liquid_transformer'
ASSET_MODEL_TEST = 'component_test.liquid_transformer_test'


class ElectricComponentLiquidTransformer(electric_component_base):
    _name = ASSET_MODEL

    _inherits = {
        'component.component': 'delegated_id',
    }

    def _component_info(self):
        res = dict.fromkeys(self.ids, "")
        for rec in self:
            value = 'Class=' + str(rec.Class) + ' ,'
            value = value +'Type=' + str(rec.Type) + ' ,'
            value = value +'PowerR=' + str(rec.PowerR) + ' ,'
            res[rec.ids[0]] = value
        return res

    # Columns
    delegated_id = fields.Many2one('component.component', required=True, ondelete='cascade')

    # ************* Equipment Data. Specific fields of Liquid Transformers ***************
    Class = fields.Char('Class:', size=20, required=False)
    Type = fields.Char('Type:', size=20, required=False)
    OilLevel = fields.Float('Oil Level:', default=0)
    PrimaryV = fields.Integer('Primary Voltage(V):', default=0)
    SecondaryV = fields.Integer('Secondary Voltage(V):', default=0)
    SerialNo = fields.Char('Serial No:', size=20, required=False)
    PowerR = fields.Integer('Power Rating(MVA):', default=0)
    Frequency = fields.Integer('Frequency(Hz):', default=0)

    TemperRise = fields.Integer('Temper. Rise(°C):', default=0)
    TypeInsul = fields.Char('Type of Insulation:', size=20, required=False)
    WConf = fields.Char('Winding Config:', size=20, required=False)
    MFGDate = fields.Date('MFG Date', size=20, required=False)
    Impedance = fields.Float('Impedance(%):', default=0)
    OilTemp = fields.Integer('Oil Temperature(°C):', default=0)
    OilTemDem = fields.Integer('Oil Temper. Demand(°C):', default=0)
    WTemp = fields.Integer('Winding Temper.(°C):', default=0)
    Tap = fields.Integer('Tap:', default=0)
    Equip_Designation = fields.Char('Equip_Designation:', size=40, required=False)
    component_info = fields.Char(compute='_component_info', string='Information')

    @api.model
    def create(self, values):
        if not values.get('electric_component_type') and 'electric_component_type' not in self._context:
            values['electric_component_type'] = ASSET_TYPE
            values['component_model'] = ASSET_MODEL
            values['component_model_test'] = ASSET_MODEL_TEST
        return super(ElectricComponentLiquidTransformer, self).create(values)


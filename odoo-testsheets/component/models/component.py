﻿# -*- coding: utf-8 -*-
##############################################################################
#
#    odoo, Open Source Management Solution
#    Copyright (C) 2013-2015 CodUP (<http://codup.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import fields, models

from odoo.tools import pycompat

from odoo import tools
from odoo import api
import base64

STATE_COLOR_SELECTION = [
    ('0', 'Red'),
    ('1', 'Chartreuse'),
    ('2', 'Blue'),
    ('3', 'Yellow'),
    ('4', 'Magenta'),
    ('5', 'Cyan'),
    ('6', 'Black'),
    ('7', 'White'),
    ('8', 'Orange'),
    ('9', 'SkyBlue')
]


class Component_state(models.Model):
    """ 
    Model for Component states.
    """
    _name = 'component.state'
    _description = 'State of Component'
    _order = "sequence"

    STATE_SCOPE_TEAM = [
        ('0', 'Finance'),
        ('1', 'Warehouse'),
        ('2', 'Manufacture'),
        ('3', 'Maintenance'),
        ('4', 'Accounting')
    ]

    # Columns
    name = fields.Char('State', size=64, required=True, translate=True)
    sequence = fields.Integer('Sequence', help="Used to order states.", default=1)
    state_color = fields.Selection(STATE_COLOR_SELECTION, 'State Color')
    team = fields.Selection(STATE_SCOPE_TEAM, 'Scope Team')

    # _defaults = {
    #     'sequence': 1,
    # }

    def change_color(self, cr, uid, ids, context=None):
        state = self.browse(cr, uid, ids[0], context=context)
        color = int(state.state_color) + 1
        if (color > 9): color = 0
        return self.write(cr, uid, ids, {'state_color': str(color)}, context=context)


class Component_category(models.Model):
    _description = 'Component Tags'
    _name = 'component.category'

    # Columns
    name = fields.Char('Tag', required=True, translate=True)
    component_ids = fields.Many2many('component.component', id1='category_id', id2='component_id', string='Components')


class Component_component(models.Model):
    """
    Components
    """
    _name = 'component.component'
    _description = 'Component'
    _inherit = ['mail.thread']

    # @api.one
    # @api.depends('image')
    # def _get_image(self):
    #     resized_images = tools.image_get_resized_images(self.image, return_big=True)
    #     self.image_medium = resized_images['image_medium']
    #     self.image_small = resized_images['image_small']
    #
    # @api.one
    # def _set_image_medium(self):
    #     self._set_image_value(self.image_medium)
    #
    # @api.one
    # def _set_image_small(self):
    #     self._set_image_value(self.image_small)
    #
    # @api.one
    # def _set_image_value(self, value):
    #     self.image = base64.b64encode(value)

    @api.one
    @api.depends('image')
    def _get_compute_images(self):
        if isinstance(self.image, pycompat.text_type):
            image = self.image.encode('ascii')
        else:
            image = self.image

        resized_images = tools.image_get_resized_images(image, return_big=True, avoid_resize_medium=True)
        self.image_medium = resized_images['image_medium']
        self.image_small = resized_images['image_small']

    def _read_group_state_ids(self, cr, uid, ids, domain, read_group_order=None, access_rights_uid=None, context=None,
                              team='3'):
        access_rights_uid = access_rights_uid or uid
        stage_obj = self.pool.get('component.state')
        order = stage_obj._order
        # lame hack to allow reverting search, should just work in the trivial case
        if read_group_order == 'stage_id desc':
            order = "%s desc" % order
        # write the domain
        # - ('id', 'in', 'ids'): add columns that should be present
        # - OR ('team','=',team): add default columns that belongs team
        search_domain = []
        search_domain += ['|', ('team', '=', team)]
        search_domain += [('id', 'in', ids)]
        stage_ids = stage_obj._search(cr, uid, search_domain, order=order, access_rights_uid=access_rights_uid,
                                      context=context)
        result = stage_obj.name_get(cr, access_rights_uid, stage_ids, context=context)
        # restore order of the search
        result.sort(lambda x, y: cmp(stage_ids.index(x[0]), stage_ids.index(y[0])))
        return result, {}

    def get_formview_id(self, cr, uid, id, context=None):
        model, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'component',
                                                                             'components_kanban_view')
        return view_id

    def _read_group_finance_state_ids(self, cr, uid, ids, domain, read_group_order=None, access_rights_uid=None,
                                      context=None):
        return self._read_group_state_ids(cr, uid, ids, domain, read_group_order, access_rights_uid, context, '0')

    def _read_group_warehouse_state_ids(self, cr, uid, ids, domain, read_group_order=None, access_rights_uid=None,
                                        context=None):
        return self._read_group_state_ids(cr, uid, ids, domain, read_group_order, access_rights_uid, context, '1')

    def _read_group_manufacture_state_ids(self, cr, uid, ids, domain, read_group_order=None, access_rights_uid=None,
                                          context=None):
        return self._read_group_state_ids(cr, uid, ids, domain, read_group_order, access_rights_uid, context, '2')

    def _read_group_maintenance_state_ids(self, cr, uid, ids, domain, read_group_order=None, access_rights_uid=None,
                                          context=None):
        return self._read_group_state_ids(cr, uid, ids, domain, read_group_order, access_rights_uid, context, '3')

    def copy_device(self):
        for component in self:
            vals = {
                'name': component.name + ' copy'
            }
            true_component = self.env[component.component_model]
            return true_component.search([('delegated_id', '=', component.id)]).copy(vals)

    def move(self, parent):
        return self.write({'parent_id': parent})

    ELECTRIC_ASSET_TYPE_SELECTION = [
        ('all_components', 'ALL Components'),
        ('lv_ac_cable', 'LV AC Cable'),
        ('board', 'Board'),
        ('lv_dc_cable', 'LV DC Cable'),
        ('hv_ac_cable', 'HV AC Cable'),
        ('lv_ac_bus', 'LV Bus'),
        ('hv_ac_bus', 'HV Bus'),
        ('hv_busjcr', 'HV Bus Joint Contact Resistance'),
        ('swr', 'SWR'),
        ('switch', 'Switch'),
        ('dry_transformer', 'Dry Transformer'),
        ('vol_transformer', 'Volt Transformer'),
        ('liquid_transformer', 'Liquid Transformer'),
        ('curent_transformer', 'Current Transformer'),
        ('pindex_transformer', 'Transformer Polarization Index'),
        ('automatic_transferswitch', 'Automatic Transfer switch'),
        ('lv_cb', 'LV Circuit Breaker'),
        # ('lv_cb', 'LV Circuit Breaker Primary Injection'), popo
        ('lv_cbeatonamp', 'LV Circuit Breaker Eaton(Ampector)'),
        ('lvdc_cb', 'LV DC Circuit Breaker'),
        ('mcb_withrelay', 'Moduled Case Breaker(with relay'),
        ('load_bs', 'Load Break Switch'),
        ('hvac_circuitbreaker', 'HV Circuit breaker'),
        ('ptop_ground', 'Point to Point Ground'),
        ('surge_arrestor', 'Surge Arrestor')
    ]

    CRITICALITY_SELECTION = [
        ('0', 'General'),
        ('1', 'Important'),
        ('2', 'Very important'),
        ('3', 'Critical')
    ]

    @api.one
    def _get_project(self):
        sites = self.site_id
        for site in self.env['component.site'].search_read([['id', '=', sites.id]],
                                                                fields=["project_id"]):
            result = site['project_id']
        self.project_id = result

    @api.one
    def _get_site(self):
        for device in self:
            for sublocation in self.env[device.parent_model].search_read([['id', '=', device.parent_id]],
                                                                              fields=["location_id"]):
                for location in self.env['component.location'].search_read([['id', '=', sublocation["location_id"][0]]], fields=["site_id"]):
                    result = location['site_id']
        self.site_id = result

    @api.one
    def _get_location(self):
        for device in self:
            if device.parent_model == 'component.sublocation':
                for sublocation in self.env['component.sublocation'].search_read([['id', '=', device.parent_id]]):
                    id = sublocation['location_id'][0]
                    for location in self.env['component.location'].search_read([['id', '=', id]]):
                        result = location
        self.location_id = result

    @api.one
    def _get_sublocation(self):
        for device in self:
            if device.parent_model == 'component.sublocation':
                for sublocation in self.env['component.sublocation'].search_read([['id', '=', device.parent_id]]):
                    result = sublocation
            else:
                result = False
        self.sublocation_id = result

    def _get_parent(self):
        sublocations = self._get_sublocation()
        for device in self:
            if sublocations[device.id]:
                for sublocation in self.env['component.sublocation'].search_read([["id", "=", sublocations[device.id]]], fields=['name']):
                    parent = parent + ',' + sublocation['name']
            result = parent
        return result

    # Columns
    name = fields.Char('Component Name', size=64, required=True, translate=True)
    finance_state_id = fields.Many2one('component.state', 'State', domain=[('team', '=', '0')])
    warehouse_state_id = fields.Many2one('component.state', 'State', domain=[('team', '=', '1')])
    manufacture_state_id = fields.Many2one('component.state', 'State', domain=[('team', '=', '2')])
    maintenance_state_id = fields.Many2one('component.state', 'State', domain=[('team', '=', '3')])
    # maintenance_state_color = fields.Related('maintenance_state_id', 'state_color', type="selection", selection=STATE_COLOR_SELECTION, string="Color", readonly=True)
    maintenance_state_color = fields.Selection(related='maintenance_state_id.state_color', string="Color",
                                               readonly=True, selection=STATE_COLOR_SELECTION)
    criticality = fields.Selection(CRITICALITY_SELECTION, 'Criticality')
    user_id = fields.Many2one('res.users', 'Assigned to', track_visibility='onchange')
    active = fields.Boolean('Active', default=True)

    manufacturer = fields.Char('Manufacturer', size=50, required=False)
    component_number = fields.Char('Component Number', size=64)
    model = fields.Char('Model', size=64)
    serial = fields.Char('Serial no.', size=64)
    vendor_id = fields.Many2one('res.partner', 'Vendor')
    start_date = fields.Date('Start Date')
    purchase_date = fields.Date('Purchase Date')
    warranty_start_date = fields.Date('Warranty Start')
    warranty_end_date = fields.Date('Warranty End')
    image = fields.Binary("Image", help="This field holds the image used as image for the Component, limited to 1024x1024px.")
    image_medium = fields.Binary(compute='_get_compute_images',
                                 # inverse='_set_image',
                                 string="Medium-sized image",
                                 help="Medium-sized image of the component. It is automatically " \
                                      "resized as a 128x128px image, with aspect ratio preserved, " \
                                      "only when the image exceeds one of those sizes. Use this field in form views or some kanban views.")
    image_small = fields.Binary(compute='_get_compute_images',
                                # inverse='_set_image',
                                string="Small-sized image",
                                help="Small-sized image of the component. It is automatically " \
                                     "resized as a 64x64px image, with aspect ratio preserved. " \
                                     "Use this field anywhere a small image is required.")
    category_ids = fields.Many2many('component.category', id1='component_id', id2='category_id', string='Tags')
    project_id = fields.Many2one('component.project', compute='_get_project', string='Project', default=False)
    site_id = fields.Many2one('component.site', compute='_get_site', string='Site', default=False)
    location_id = fields.Many2one('component.location', compute='_get_location', string='Location')
    sublocation_id = fields.Many2one('component.sublocation', compute='_get_sublocation', string='Sublocation')
    electric_component_type = fields.Selection(ELECTRIC_ASSET_TYPE_SELECTION, string="Type", readonly=False,
                                               required=True, help="Type of the electric component.")

    parent_model = fields.Char('Parent Model', readonly=True,
                               help="The Location or Sublocation this Device will be attached to")
    parent_field = fields.Char('Parent Field', readonly=True)
    parent_id = fields.Integer('Parent', readonly=True, help="The record id this is attached to")

    # parent = fields.Char(compute='_get_parent', string='Parent')
    single_line_diagram_hotpoint = fields.Char(string="Single Line Diagram Hot Point",
                                               help="This field holds the Vector feature for hot zones that represent the Device on a SLD")
    electric_board_hotpoint = fields.Char(string="Electric Board Hot Point",
                                          help="This field holds the Vector feature for hot zones that represent the Device on a Eletric board")

    # _defaults = {
    #     'active': True,
    #     'site_id': False,
    #     'project_id': False,
    # }

    _group_by_full = {
        'finance_state_id': _read_group_finance_state_ids,
        'warehouse_state_id': _read_group_warehouse_state_ids,
        'manufacture_state_id': _read_group_manufacture_state_ids,
        'maintenance_state_id': _read_group_maintenance_state_ids,
    }

    # @api.model
    # def create(self, vals):
    #     tools.image_resize_images(vals)
    #     return super(Component_component, self).create(vals)
    #
    # @api.multi
    # def write(self, vals):
    #     tools.image_resize_images(vals)
    #     return super(Component_component, self).write(vals)
